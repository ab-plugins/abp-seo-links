<?php
/**
 * SEO Plugin for MyBB 1.8
 * Copyright � 2006 Asad Niazi, All Rights Reserved!
 * Copyright � 2018 CrazyCat
 *
 * Website: http://www.spicefuse.com
 * License: 
 * This plugin is offered "as is" with no guarantees.
 * You may redistribute it provided the code and credits 
 * remain intact with no changes. This is not distributed
 * under GPL, so you may not re-use the code in any other
 * module, plugin, or program. 
 * 
 * Free for non-commercial purposes!
 *
 */

// remove search engine unfriendly links for guests/bots etc..?
// read a note about it -- currently not in use..
// recommendation: Keep it off!
define("REMOVE_UNF_LINKS", 0);

// add rel="nofollow" on SE unfriendly links?
// recommendation: Keep it on!
define("NOFOLLOW_UNF_LINKS", 1);

// force redirects to proper url on showthread.php? 
// This forces a Location: redirect to the properly
// formatted, serp friendly url
// recommendation: Keep it on!
define("FORCE_REDIRECT", 1);

// comment it to disable this mod while keeping the 
// dependencies happy!
$plugins->add_hook("pre_output_page", "seo_change_links");

// force redirect?
if (FORCE_REDIRECT == 1) {
	$plugins->add_hook("global_start", "seo_change_request");
}

function seolinks_info()
{
	return array(
		"name"			=> "SEO Links!",
		"description"	=> "A plugin that does the SEO for links.",
		"website"		=> "https://www.g33k-zone.org",
		"author"		=> "CrazyCat",
		"authorsite"	=> "https://www.g33k-zone.org",
		"version"		=> "1.2",
		"codename"		=> "seolinks",
		"compatibility"	=> "18*"
	);
}

function seolinks_activate()
{

	$htaccess_code = "\r\n# //seo_mod_start\r\n"
				   . "%extra_code%"
				   . "# Uncomment the following and add your forum path if rewrites arent working properly\r\n"
				   . "#RewriteBase /mybb/\r\n"
				   . "RewriteRule ^index.html$ index.php [L,NE]\r\n"
				   . "RewriteRule ^(.*)-t-([0-9]+).html(.*)$ showthread.php?tid=\$2\$3 [QSA,L]\r\n"
				   . "RewriteRule ^(.*)-t-([0-9]+)-([0-9]+).html$ showthread.php?tid=\$2&page=\$3 [QSA,L]\r\n"
				   . "RewriteRule ^(.*)-f-([0-9]+).html(.*)$ forumdisplay.php?fid=\$2\$3 [QSA,L]\r\n"
				   . "RewriteRule ^(.*)-f-([0-9]+)-([a-z]+)(-|-[a-z]+)-([0-9]+)-([0-9]+).html(.*)$ forumdisplay.php?fid=\$2&sortby=\$3&order=\$4&datecut=\$5&page=\$6\$7 [L]\r\n"
				   . "RewriteRule ^(.*)-a-([0-9]+).html$ announcements.php?aid=\$2 [L]\r\n"
				   . "# //seo_mod_end\r\n";
	
	if (!file_exists('../.htaccess')) {
		@touch('../.htaccess');
	}
	
	if (is_writeable("../.htaccess")) 
	{
		$cur_htaccess = implode("", file("../.htaccess"));
		
		// part of all of it already exists?
		if (strstr($cur_htaccess, "//seo_mod_start")) {
			return;
		}
		
		// rewrite engine on?		
		$htaccess_code = str_replace("%extra_code%", (preg_match("#RewriteEngine.+?on#i", $cur_htaccess) ? "" : "RewriteEngine On\r\n"), $htaccess_code);
		
		$fp = fopen("../.htaccess", 'w');
		fwrite($fp, $cur_htaccess . $htaccess_code);
		fclose($fp);
		
		return;
	}

	// we're a bit screwed, the following message will exit he execution
	// so, Update plugin cache here
	global $plugins_cache, $cache, $active_plugins, $plugins_cache, $message;
	$plugins_cache['active'] = $active_plugins;
	$cache->update("plugins", $plugins_cache);
	
	$message = "<b>Copy/Paste the following code at end of a .htaccess file in your forum root directory:</b><br /><br />"
					. "<textarea cols='90' rows='8'>". str_replace("%extra_code%", "RewriteEngine On\r\n", $htaccess_code) ."</textarea>"
					. "<br /><center><a href=\"plugins.php\">Go Back to Plugins</a></center>";
}

function seolinks_deactivate()
{
	if (is_writeable("../.htaccess"))
	{
		// remove the seo plugin feed mod_rewrite stuff
		$htaccess_code = implode("", file("../.htaccess"));
		$code_start = substr($htaccess_code, 0, strpos($htaccess_code, "# //seo_mod_start"));
		$code_end = substr($htaccess_code, strpos($htaccess_code, "# //seo_mod_end") + strlen("# //seo_mod_end"));
		$merge_code = $code_start . $code_end;
		
		// prepend rewriteengine on if it only existed within the 
		// code that's stripped out..
		if (!preg_match("#RewriteEngine.+?on#i", $merge_code) && strlen($merge_code) > 15) {
			$merge_code = "RewriteEngine On\r\n\r\n" . $merge_code;
		}
		
		// write to htaccess
		$fp = fopen("../.htaccess", 'w');
		fwrite($fp, $merge_code);
		fclose($fp);
	}
}

/**
 * A kinda ugly function that I built for showthread.php
 * It redirects to the right URL if a topic is visited
 * directly somehow. 
 * 
 * Excuse me if it doesnt makes much sense, just the 
 * result of some final touches to original work.
 * 
 * @access private
 */
function seolinks_change_request()
{
	global $db, $mybb, $session;
	
	$basename = basename($_SERVER['REQUEST_URI']);
	if (substr($basename, 0, strpos($basename, '?')) == "showthread.php") 
	{
		$query_str = explode("&", $_SERVER['QUERY_STRING']);
		foreach ($query_str as $var) {
			$var = explode("=", $var);
			$query_vars[$var[0]] = $var[1];
		}
		
		$query_vars['tid'] = intval($query_vars['tid']);
		
		// we need at least topic or post id -- dont go looking for the last page here
		if (($query_vars['tid'] == "" && $query_vars['pid'] == "") OR $query_vars['page'] == 'last' OR $query_vars['mode'] == 'threaded') 
		{
			return;
		}
		if (isset($query_vars['page']) && $query_vars['page']!='last') {
			$query_vars['page'] = intval($query_vars['page']);
			if ($query_vars['page']==0) $query_vars['page'] = 1;
		}
		// kill action variable for bots
		if ($query_vars['pid'] == "" && ($query_vars['action'] == "" OR $session->is_spider == true))
		{
			if (($title_url = seo_fetch_topic_title($query_vars['tid'])) != false) 
			{
				ob_clean(); // flush current output..
				header("Location: {$mybb->settings['bburl']}/{$title_url}-t-{$query_vars['tid']}".(($query_vars['page'] != "" && $query_vars['page'] != 1) ? "-{$query_vars['page']}" : "") . ".html" . $query_extra);
				exit();
			}
		}
		// handle pids
		else if ($query_vars['pid'] != "")
		{
			if (($fetch_data = seo_fetch_topic_title(0, $query_vars['pid'])) != false)  
			{
				if (!is_array($fetch_data)) {
					return;
				}
				
				ob_clean(); // flush current output..
				header("Location: {$mybb->settings['bburl']}/{$fetch_data[subject]}-t-{$query_vars['tid']}".(($fetch_data['page'] != "" && $fetch_data['page'] != 1) ? "-{$fetch_data['page']}" : "") . ".html#pid{$query_vars[pid]}");
				exit();
			}
		} // end pid
	}
}

/**
 * Main function that does all the replacement of links with 
 * the special keyword filled links. (usually based on titles/subjects) 
 * 
 * Currently replaces the following types of links:
 *  Forum display links (forumdisplay.php) - where required
 *  Threads links (showthread.php) - where required
 *  Announcement links (announcements.php) - on forum display
 *  Adds rel="nofollow" to many links which arent SE friendly
 * 
 * @access private
 */
function seo_change_links($page)
{
	global $data_cache, $fcache, $forum, $threadcache, $mybb, $db;

	/*$mtime1 = explode(" ", microtime());
	$start_time = $mtime1[1] + $mtime1[0];
	*/
	
	$mybb->settings['cur_file'] = basename($_SERVER['PHP_SELF']);
	
	// find all forum ids now
	preg_match_all("#forumdisplay.php\?fid=([0-9]+)#", $page, $forum_matches);
	for ($i = 0; $i < count($forum_matches[1]); $i++)
	{
		$data_cache['forums'][] = $forum_matches[1][$i];
	}
	
	// query topic titles of similar threads at thread view, for cache!
	// if this one extra query seems heavy to you, custom modification 
	// of the showthread.php is always possible..
	if ($mybb->settings['showsimilarthreads'] != "no" && $mybb->settings['cur_file'] == "showthread.php" && $mybb->input['tid'] != "") 
	{
		// find all topic ids now
		preg_match_all("#showthread.php\?tid=([0-9]+)#", $page, $topic_matches);
		for ($i = 0; $i < count($topic_matches[1]); $i++)
		{
			if ($topic_matches[1][$i] == $mybb->input['tid']) {
				continue;
			}
			
			$data_cache['topics'][] = $topic_matches[1][$i];
		}

		if (is_array($data_cache['topics']))
		{
			$ids_str = implode(",", $data_cache['topics']);
			$query = $db->query("SELECT subject, tid FROM ".TABLE_PREFIX."threads WHERE tid IN({$ids_str})");
			while ($fetch = $db->fetch_array($query)) {
				$threadcache[$fetch['tid']]['subject'] = $fetch['subject'];
			}
		}
	}
	
	// replace forum urls..
	$page = preg_replace_callback("#forumdisplay.php\?fid=([0-9]+)&sortby=([a-z]+)(.*)order.*datecut.*page=([0-9]+).*(\"|')#i", function ($matches) { return seo_forum_url($matches[1], 2, $matches[0], array($matches[5])); }, $page);
	$page = preg_replace_callback("#forumdisplay.php\?fid=([0-9]+)(\"|')#", function ($matches) { return seo_forum_url($matches[1], 1, $matches[0], array($matches[2])); }, $page);
	
	// replace topic urls
	$page = preg_replace_callback("#showthread.php\?tid=([0-9]+)(\"|')#", function ($matches) { return seo_topic_url($matches[1], 1, $matches[0], $matches[2]); }, $page);
	$page = preg_replace_callback("#showthread.php\?tid=([0-9]+)(&amp;|&)page=([0-9]+)(\"|')#", function ($matches) { return seo_topic_url($matches[1], 2, $matches[0], $matches[4], $matches[3]); }, $page);
	
	// remove few links for guests..
	// Note: While I was working on it, I decided to use robots.txt 
	// for the very same purpose. Removing some functionality
	// for guests doesn't sounds too nice. For now, I will let it
	// live in here and later decide whether to improve the following
	// code or not, depending upon user feedback.
	if ($mybb->user['usergroup'] == 1 && REMOVE_UNF_LINKS == 1) 
	{
		$page = preg_replace("#<a.*href=(\"|')showthread.php\?tid=([0-9]+)(&amp;|&)action=lastpost(\"|')(|.+?)>(.+?)</a>#", "\\6", $page);
		$page = preg_replace("#<a.*href=(\"|')showthread.php\?action=lastpost(&amp;|&)tid=([0-9]+)(\"|')(|.+?)>(.+?)</a>#", "\\6", $page);
	}
	
	// After the above idea of removing all unfrinedly links didnt 
	// sound too good to me, I got another idea of adding a  
	// rel="nofollow" on the unfriendly links. Plus, the robots.txt 
	// file will be used for backward compatibility of search engines. 
	// What say? Suggestions at http://spicefuse.com
	if ($mybb->user['usergroup'] == 1 && NOFOLLOW_UNF_LINKS == 1)
	{
		$page = preg_replace_callback("#(<a.+?href=.+?(search|stats|usercp2|printthread|sendthread|showteam|memberlist|calendar|member|online|private|newreply|newthread|showthread|forumdisplay)\.php)(.+?)>(.+?)</a>#", function ($matches) { return seo_add_nofollow($matches[1], $matches[3], $matches[4]); }, $page);
	}
	
	// after adding the search engine no follow, now let's convert
	// few to SE friendly urls anyways (incase a user copies and pastes it somewhere)
	//   - showthread.php?tid=20&amp;pid=20#pid20
	$page = preg_replace_callback("#showthread.php\?tid=([0-9]+)(&amp;|&)pid=([0-9]+)(\#pid[0-9]+|)(\"|')#", function ($matches) { return seo_topic_url($matches[1], 3, $matches[0], $matches[5], $matches[3], array($matches[4])); }, $page);
	
	// remove site name from page title and take care of announcements if we are at forum display
	if ($mybb->settings['cur_file'] == 'forumdisplay.php' && $mybb->input['fid'] != "")
	{
		$page = preg_replace_callback("#<title>{$mybb->settings[bbname]}.+?-(.+?)</title>#", function ($matches) { return seo_page_title($matches[1]); }, $page);
		
		global $announcements;
		if ($announcements != "")
		{
			$page = preg_replace_callback("#(<a.+?href=.+?)announcements.php\?aid=([0-9]+)(.+?>)(.+?)</a>#", function ($matches) { return seo_announcement_url($matches[2], $matches[4], array($matches[1], $matches[3])); }, $page);
		}
	}

	/*$mtime2 = explode(" ", microtime());
	$currenttime = $mtime2[1] + $mtime2[0];
	$totaltime = $currenttime - $start_time;
	echo "<br />SEO Plugin took: {$totaltime} seconds<br /><br />";*/
	
	return $page;
}

/**
 * Internal function to generate forums URLs
 * based on data provided.
 * 
 * @access private
 */
function seo_forum_url($forum_id, $type = 1, $current, $extra_data = array())
{
	global $forumcache, $sp_fcache, $data_cache, $db;

	// try to build forumcache if we don't have it
	if (!is_array($forumcache))
	{
		cache_forums();
		$forumcache = array(); // prevent unexpected infinite loops
		seo_forum_url($forum_id, $type, $current, $extra_data);
	}
	
	// build our specially formatted sp_fcache array
	if (is_array($forumcache) && !is_array($sp_fcache))
	{
		$sp_fcache = seo_build_fcache($forumcache);
	}
	
	// do we have the forum cache?
	if (is_array($sp_fcache))
	{
		if (is_array($sp_fcache[$forum_id])) {
			$title_url = seo_clean_title($sp_fcache[$forum_id]['name']);
		}
	}
	else
	{
		if (!is_array($data_cache['forum_names'])) 
		{
			$data_cache['forum_names'] = array(); // prevent infinite loops
			
			$forum_ids = implode(",", $data_cache['forums']);
			$query = $db->query("SELECT f.fid, f.name FROM ".TABLE_PREFIX."forums f WHERE f.fid IN({$forum_ids})");
			while ($fetch = $db->fetch_array($query))
			{
				$data_cache['forum_names'][$fetch['fid']] = $fetch['name'];
			}
			
			seo_forum_url($forum_id, $type, $current, $extra_data);
		}
		else
		{
			if ($data_cache['forum_names'][$forum_id] != "") {
				$title_url = seo_clean_title($data_cache['forum_names'][$forum_id]);
			}
		}
	}
	
	// return title_url if we have it
	if ($title_url != "") 
	{
		if ($type == 1) {
			return $title_url . "-f-{$forum_id}.html" . stripslashes($extra_data[0]);
		}
		else if ($type == 2) 
		{
			// some dirty data fetching..
			preg_match("/(&amp;|&|)datecut=([0-9]+)(&amp;|&|$)/", $current, $match);
			$date_cut = $match[2];
			
			preg_match("/(&amp;|&|)order=([a-zA-Z]+)(&amp;|&|$)/", $current, $match);
			$order = $match[2];
			
			preg_match("/(&amp;|&|)sortby=([a-z]+)/", $current, $match);
			$sort_by = $match[2];
			
			preg_match("/(&amp;|&|)page=([0-9]+)/", $current, $match);
			$page = $match[2];
			
			return $title_url . "-f-{$forum_id}-{$sort_by}-{$order}-{$date_cut}-{$page}.html" . stripslashes($extra_data[0]);
		}
	}
	else
	{
		return stripslashes($current);
	}
}

/**
 * Internal function to generate thread URLs
 * based on data provided.
 * 
 * @access private
 */
function seo_topic_url($topic_id, $type = 1, $current, $delimter = "", $page_no = 0, $extra_data = array())
{
	global $thread, $threadcache, $mybb;

	if ($threadcache[$topic_id]['subject'] != "") {
		$topic_url = seo_clean_title($threadcache[$topic_id]['subject']);
	}
	else if ($mybb->settings['cur_file'] == 'showthread.php' && $thread['subject'] != "")  {
		$topic_url = seo_clean_title($thread['subject']);
	}
	else
	{
		return stripslashes($current);
	}


	if ($type == 1)	{
		return $topic_url . "-t-{$topic_id}.html" . stripslashes($delimter);
	}
	else if ($type == 2) {
		return $topic_url . "-t-{$topic_id}-{$page_no}.html" . stripslashes($delimter);
	}
	else if ($type == 3) 
	{
		if ($mybb->input['page']!='last') $mybb->input['page'] = intval($mybb->input['page']);
		if ($mybb->input['page'] == 0) {
			$mybb->input['page'] = 1;
		}
		
		return $topic_url . "-t-{$topic_id}-{$mybb->input[page]}.html" . stripslashes($extra_data[0]) . stripslashes($delimter);
	}
}

/**
 * Internal function to generate announcement URLs
 * based on data provided.
 * 
 * @access private
 */
function seo_announcement_url($announce_id, $announce_title, $extra_data = array())
{
	$announcement_url = seo_clean_title($announce_title) . "-a-{$announce_id}.html";
	return stripslashes($extra_data[0]). $announcement_url . stripslashes($extra_data[1]) . stripslashes($announce_title) ."</a>";
}

/**
 * Internal function to re-generate page title!
 * 
 * @access private
 */
function seo_page_title($page_title)
{
	return "<title>". trim($page_title) ."</title>";
}

/**
 * Adds rel="nofollow" in the data provided by matches 
 * in parts.
 * 
 * @access private
 */
function seo_add_nofollow($part_1, $part_2, $part_3)
{
	return stripslashes($part_1) . stripslashes($part_2) . ' rel="nofollow">'. stripslashes($part_3) .'</a>';
}

/**
 * Mainly used for fetching the topic title when an id 
 * is given, but also used for getting the topic title 
 * and page number when a pid is given. 
 * 
 * Note: Return topic title is suitable only for links.
 * 
 * @access public
 */
function seo_fetch_topic_title($topic_id = 0, $pid = 0)
{
	global $db, $mybb, $ismod;
	
	if (!($topic_id XOR $pid)) {
		return false;
	}
	
	// we have a pid?
	// calculate the page and fetch the title
	$pid = intval($pid);
	if ($pid != 0) 
	{
		// get topic id and subject..
		$query = $db->query("SELECT t.tid, t.subject FROM ".TABLE_PREFIX."posts p 
								LEFT JOIN ".TABLE_PREFIX."threads t ON(p.tid = t.tid) WHERE p.pid = {$pid}");
		$fetch = $db->fetch_array($query);
			
		if (($topic_id = $fetch['tid']) == "" OR $fetch['subject'] == "") {
			return false;
		}

		// taken from showthread.php -- this part of code in
		// showthread.php will become useless!
		
		// Work out if we're showing both approved and unapproved threads or just approved..
		if($ismod)
		{
			$visible = "AND (visible='0' OR visible='1')";
		}
		else
		{
			$visible = "AND visible='1'";
		}
	
		$perpage = $mybb->settings['postsperpage'];
		$query = $db->query("SELECT COUNT(pid) as c_pid FROM ".TABLE_PREFIX."posts WHERE tid='$topic_id' AND pid <= '{$pid}' $visible");
		$result = $db->fetch_field($query, 'c_pid');
		if (($result % $perpage) == 0) {
			$page = $result / $perpage;
		} else {
			$page = intval($result / $perpage) + 1;
		}
			
		// return an array with topic subject and page number
		return array('subject' => seo_clean_title($fetch['subject']), 'page' => $page);
			
	}
	else
	{
		$query = $db->query("SELECT subject FROM ".TABLE_PREFIX."threads WHERE tid = '{$topic_id}'");
		$fetch = $db->fetch_array($query);
				
		if ($fetch['subject'] == "") {
			return false;
		}

		$title_url = seo_clean_title($fetch['subject']);
		return $title_url;
	}
}

/**
 * Generate special type of forum cache which is rather
 * easier to use and understand. 
 * 
 * @access public
 */
// is RSS plugin also used? (the function might be created)
if (!function_exists("seo_build_fcache"))
{
	function seo_build_fcache($forumcache, $full_info = 0)
	{
		@reset($forumcache);
		while (list($key,$val) = @each($forumcache))
		{
			if ($full_info == 1) {
				$sp_fcache[$val['fid']] = $val;
			} else {
				$sp_fcache[$val['fid']]['name'] = $val['name'];
			}
		}
	
		return $sp_fcache;
	}
}


/**
 * Small function to have the good "online.php" stats
 * It un-SEO the user location
 */
$plugins->add_hook("online_user", "seo_adapt_online");
function seo_adapt_online() {
	global $user;
	$pattern = '!(.*)-([atf])-([0-9]+)(-.*)?.html!';
	if (preg_match($pattern, $user['location'], $matches)) {
		switch($matches[2]) {
			case 'f':
				$user['location'] = '/forumdisplay.php?fid='.$matches[3];
				break;
			case 't':
				$user['location'] = '/showthread.php?tid='.$matches[3];
				break;
			default:
				$user['location'] = '/index.php?'.$matches[2].'id='.$matches[3];
				break;
		}
	}
}

/**
 * Clean the title strings to be used as file names in 
 * links, where required. 
 * 
 * @access private
 */
function seo_clean_title($title)
{
	// do the replacements and return encoded url
	$title = utf8_decode($title);
	$title = str_replace("&amp;", "&", $title);
	$title = preg_replace("/&([a-z]+);/", "", $title); // remove html entities such as &amp; &quot etc..
	$title = str_replace(array(":", "?", ".", "!", "$", "^", "*", ",", ";", "'", '"', "%", "~", "@", "#", "[", "]", "<", ">", "\\", "/", "=", "+"), "", $title);
    $title = str_replace(Array("�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "_", " ", "&"), Array("e", "e", "e", "e", "c", "a", "a", "i", "i", "u", "u", "o", "o", "-", "-", "and"), $title);
	$title = preg_replace("/(-+)/", "-", $title);
	return rawurlencode($title);
}

?>
